package service;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ast.Program;
import sb3.parser.xml.Sb3XmlParser;
import util.ReachableStmtAnalysis;

@RestController
public class CoverageAnalysisController {

	@CrossOrigin
	@PostMapping(value = "/analysis/coverage/{projectId}")
	public AnalysisResult analyzeCoverage(@PathVariable("projectId") String id, @RequestBody String xml) {
		AnalysisResult result = null;
		try {
			// TODO: parse XML AS program
			Sb3XmlParser sb3Parser = new Sb3XmlParser();
			Program program = sb3Parser.parseProgram(xml);
			// TODO: call method to get coverage info
			ReachableStmtAnalysis analysis = new ReachableStmtAnalysis(program);
			int count = analysis.getNumReachbleStmts();
			result = new AnalysisResult(id, count);
		} catch (Exception e) {
			result = new AnalysisResult(id, null);
		}
		return result;
	}
}