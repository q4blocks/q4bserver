package service;

public class ServerMsg {

	public String body;

	public ServerMsg() {
	}

	public ServerMsg(String body) {
		this.body = body;
	}

	public String getBody() {
		return this.body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
