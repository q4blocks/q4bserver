package service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Lists;

import ast.Program;
import data.ProjectAnalysisReport;
import main.ExprCloneImprovableFinder;
import main.ImprovableFinder;
import main.SeqOfStmtCloneImprovableFinder;
import refactor.RefactoringConfig;
import sb3.parser.xml.Sb3XmlParser;

@RestController
public class ImprovableDiscoveryServiceController {
	private static final Logger logger = LogManager.getLogger(ImprovableDiscoveryServiceController.class);
	Sb3XmlParser sb3Parser = new Sb3XmlParser();

	@CrossOrigin
	@PostMapping("/discover")
	public String analyze(@RequestBody String xml, @RequestHeader("id") String id, @RequestHeader("type") String type, @RequestHeader("evalMode") Boolean evalMode) {
		File programFile = new File("src/main/resources/last-analyzed-program.xml");
		try {
			FileUtils.writeStringToFile(programFile, xml, StandardCharsets.UTF_8.name());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println(id);
		System.out.println(type);
		System.out.println(evalMode);
		String resp;
		ProjectAnalysisReport projectReport = null;
		try {
			Program program = sb3Parser.parseProgram(xml);
			List<String> ids = Lists.newArrayList();
//			program.blockColl().stream().forEach(b->ids.add("\'"+b.getBlockId()+"\'"));
//			System.out.println(String.join(",", ids));
			ImprovableFinder finder = null;
			RefactoringConfig refacConfig = new RefactoringConfig();
			refacConfig.setEnableInvariantChecking(evalMode);
			refacConfig.setEvalMode(evalMode);
			switch (type) {
			case "duplicate-expression":
				finder = new ExprCloneImprovableFinder(program,refacConfig);
				projectReport = finder.process();
				break;
			case "duplicate-code":
				finder = new SeqOfStmtCloneImprovableFinder(program);
				projectReport = finder.process();
				break;
			default:
				throw new Exception("Unknown request type");
			}

			resp = projectReport.toJson();
			System.out.println(resp);
		} catch (Exception e) {
			e.printStackTrace(System.out);
			resp = "{\"error\":\"" + e.getClass() + "\"}";
		}
		return resp;
	}

}
