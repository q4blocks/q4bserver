package service;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AnalysisResult {
	String projectId;
	Integer estimatedCoverage;

	// TODO: change to lombok annotation
	public AnalysisResult(String projectId, Integer estimatedCoverage) {
		this.projectId = projectId;
		this.estimatedCoverage = estimatedCoverage;
	}
	
	@JsonProperty("estimated_covarage")
	public Integer getEstimatedCoverage() {
		return estimatedCoverage;
	}

	public void setEstimatedCoverage(Integer estimatedCoverage) {
		this.estimatedCoverage = estimatedCoverage;
	}

	@JsonProperty("_id")
	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
}
