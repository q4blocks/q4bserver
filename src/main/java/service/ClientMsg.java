package service;

public class ClientMsg {
	public ClientMsg() {

	}

	private String body;

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "ClientMsg [" + (body != null ? "body=" + body : "") + "]";
	}

}
