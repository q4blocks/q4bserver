package service;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Ignore;
import org.junit.Test;

import ast.Program;
import data.ProjectAnalysisReport;
import main.ExprCloneImprovableFinder;
import main.MetricsKeyValue;
import sb3.parser.xml.Sb3XmlParser;

public class TestFixingBug {
	@Ignore
	@Test
	public void test() throws IOException {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("last-analyzed-program.xml").getFile());
		String xml = FileUtils.readFileToString(file);
		Program program = new Sb3XmlParser().parseProgram(xml);
		ExprCloneImprovableFinder finder = new ExprCloneImprovableFinder(program);
		ProjectAnalysisReport projectReport = finder.process();
		MetricsKeyValue metrics = projectReport.getProjectMetricsKV();
		System.out.println(projectReport.toJson());
	}

}
